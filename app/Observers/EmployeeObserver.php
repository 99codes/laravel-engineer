<?php

namespace App\Observers;

use App\Mail\EmployeeSavedMail;
use App\Models\Employee;
use Illuminate\Support\Facades\Mail;

class EmployeeObserver
{
    public function saved(Employee $employee)
    {
        Mail::to($employee->email)->send(new EmployeeSavedMail($employee));
    }
}
