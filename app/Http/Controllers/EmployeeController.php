<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreEmployeesRequest;
use App\Jobs\RegisterEmployeesJob;
use App\Models\Employee;
use Illuminate\Http\JsonResponse;

class EmployeeController extends Controller
{
    public function get(): JsonResponse
    {
        return response()->json(
            Employee::where('user_id', auth()->user()->id)->get()
        );
    }

    public function show(Employee $employee): JsonResponse
    {
        $this->authorize('employee', $employee);

        return response()->json(
            $employee
        );
    }

    public function destroy(Employee $employee): JsonResponse
    {
        $this->authorize('employee', $employee);

        return response()->json([
            'Success' => $employee->delete()
        ]);
    }

    public function store(StoreEmployeesRequest $request): JsonResponse
    {
        $employees = collect(
            $request->get('employees')
        );
        
        RegisterEmployeesJob::dispatch($employees);

        return response()->json([
            'message' => 'Registration started'
        ]);
    }
}
