<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class StoreEmployeesRequest extends FormRequest
{
    /**
     * Indicates if the validator should stop on the first rule failure.
     *
     * @var bool
     */
    protected $stopOnFirstFailure = true;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation()
    {
        if (!$this->hasFile('file')) {
            return false;
        }

        $filePath = Storage::putFileAs('uploads', $this->file('file'), sprintf('%s.csv', Str::uuid()->__toString()) );

        $fileContents = Storage::get($filePath);

        $contents = collect(
            explode("\n", $fileContents)
        );

        $header = explode(',', $contents->shift());

        $rows = $contents->map(function ($row) use ($header) {
            return collect(explode(',', $row))
                ->mapWithKeys(fn ($column, $key) => [
                    $header[$key] => trim($column)
                ])
                ->put('user_id', $this->user()->id)
                ->toArray();
        });

        $this->merge([
            'employees' => $rows->toArray()
        ]);

        Storage::delete($filePath);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'file' => ['required', 'file', 'mimes:csv'],
            'employees.*.name' => ['required'],
            'employees.*.email' => ['required', 'email'],
            'employees.*.document' => ['required', 'numeric'],
            'employees.*.city' => ['required'],
            'employees.*.state' => ['required'],
            'employees.*.start_date' => ['required', 'date'],
        ];
    }

    /**
     * Handle a failed validation attempt.
     *
     * @param  \Illuminate\Contracts\Validation\Validator  $validator
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function failedValidation(Validator $validator)
    {
        abort(406, 'NOT ACCEPTABLE: ' . $validator->errors()->first());
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'file.required' => 'Missing File',
            'file.file' => 'Invalid Data Type',
            'file.mimes' => 'Invalid File Extension',
            'employees.*.name.required' => 'Missing Name',
            'employees.*.email.required' => 'Missing E-mail Address',
            'employees.*.email.email' => 'Invalid E-mail Address',
            'employees.*.document.required' => 'Missing Document',
            'employees.*.document.numeric' => 'Invalid Document Type',
            'employees.*.city.required' => 'Missing City',
            'employees.*.state.required' => 'Missing State',
            'employees.*.start_date.required' => 'Missing Date',
            'employees.*.start_date.date' => 'Invalid Date',
        ];
    }
}
