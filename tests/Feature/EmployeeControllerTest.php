<?php

namespace Tests\Feature;

use App\Mail\EmployeeSavedMail;
use App\Models\Employee;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Laravel\Passport\Passport;
use Tests\TestCase;

class EmployeeControllerTest extends TestCase
{
    use RefreshDatabase;

    protected User $user;

    protected function setUp(): void
    {
        parent::setUp();

        $this->user = Passport::actingAs(
            User::factory()->create(),
            ['create-servers']
        );
    }

    public function test_it_gets_all_employees_with_a_auth_user()
    {
        $employees = Employee::factory(3)->create([
            'user_id' => $this->user->id
        ]);

        $this->get(route('employees.get'))
            ->assertOk()
            ->assertJson(
                $employees->map(function ($employee) {
                    return [
                        'id' => $employee->id,
                        'user_id' => $employee->user_id,
                        'name' => $employee->name,
                        'email' => $employee->email,
                        'document' => $employee->document,
                        'city' => $employee->city,
                        'state' => $employee->state,
                        'start_date' => $employee->start_date,
                    ];
                })->toArray()
            );
    }

    public function test_it_cannot_gets_employees_from_another_user()
    {
        $user = User::factory()->create();

        Employee::factory(3)->create([
            'user_id' => $user->id
        ]);

        $this->get(route('employees.get'))
            ->assertOk()
            ->assertExactJson([]);
    }

    public function test_it_shows_a_employee_with_a_auth_user()
    {
        $employee = Employee::factory()->create([
            'user_id' => $this->user->id
        ]);

        $this->get(route('employees.show', [$employee->id]),)
            ->assertOk()
            ->assertJson([
                'id' => $employee->id,
                'user_id' => $employee->user_id,
                'name' => $employee->name,
                'email' => $employee->email,
                'document' => $employee->document,
                'city' => $employee->city,
                'state' => $employee->state,
                'start_date' => $employee->start_date,
            ]);
    }

    public function test_it_cannot_shows_a_employee_from_another_user()
    {
        $user = User::factory()->create();

        $employee = Employee::factory()->create([
            'user_id' => $user->id
        ]);

        $this->get(route('employees.show', [$employee->id]))
            ->assertForbidden()
            ->assertSee('This action is unauthorized.');
    }

    public function test_it_deletes_a_employee_with_a_auth_user()
    {
        $employee = Employee::factory()->create([
            'user_id' => $this->user->id
        ]);

        $this->assertDatabaseHas('employees', [
            'id' => $employee->id
        ]);

        $this->delete(route('employees.destroy', [$employee->id]),)
            ->assertOk()
            ->assertExactJson([
                'Success' => true
            ]);

        $this->assertDatabaseMissing('employees', [
            'id' => $employee->id
        ]);
    }

    public function test_it_cannot_deletes_a_employee_from_another_user()
    {
        $user = User::factory()->create();

        $employee = Employee::factory()->create([
            'user_id' => $user->id
        ]);

        $this->assertDatabaseHas('employees', [
            'id' => $employee->id,
            'user_id' => $user->id
        ]);

        $this->delete(route('employees.destroy', [$employee->id]),)
            ->assertForbidden()
            ->assertSee('This action is unauthorized.');

        $this->assertDatabaseHas('employees', [
            'id' => $employee->id,
            'user_id' => $user->id
        ]);
    }

    public function test_it_stores_employees_from_an_uploaded_csv_file()
    {
        Storage::fake('local');
        Mail::fake();

        $rows = [
            'name,email,document,city,state,start_date',
            'Bob Wilson,bob@paopaocafe.com,13001647000,Salvador,BA,2020-01-15',
            'Laura Matsuda,lm@matsuda.com.br,60095284028,Niterói,RJ,2019-06-08',
            'Marco Rodrigues,marco@kyokugen.org,71306511054,Osasco,SC,2021-01-10',
            'Christie Monteiro,monteiro@namco.com,28586454001,Recife,PE,2015-11-03',
        ];

        $response = $this->post(route('employees.store'),  [
            'file' => UploadedFile::fake()->createWithContent('employees.csv', implode("\n", $rows))
        ], [
            'Accept' => 'application/json'
        ]);

        $response->assertOk();

        $this->assertDatabaseCount('employees', 4);

        Mail::assertSent(EmployeeSavedMail::class, 4);
    }

    public function test_it_updates_employees_from_an_uploaded_csv_file()
    {
        Storage::fake('local');
        Mail::fake();

        $employee = Employee::create([
            'name' => 'Marcos Rodrigues',
            'email' => 'marco@kyokugen.org',
            'document' => 71306511054,
            'city' => 'Osasco',
            'state' => 'SP',
            'start_date' => '2021-01-10',
            'user_id' => $this->user->id,
        ]);

        $rows = [
            'name,email,document,city,state,start_date',
            'Marco Rodrigues,marco@kyokugen.org,71306511054,Osasco,SP,2021-01-10',
        ];

        $response = $this->post(route('employees.store'),  [
            'file' => UploadedFile::fake()->createWithContent('employees.csv', implode("\n", $rows))
        ], [
            'Accept' => 'application/json'
        ]);

        $response->assertOk();

        $this->assertDatabaseHas('employees', [
            'document' => $employee->document,
            'state' => 'SP',
        ]);

        Mail::assertSent(EmployeeSavedMail::class);
    }

    public function test_it_cannot_store_employees_from_an_uploaded_csv_file_when_validation_fails()
    {
        Storage::fake('local');

        $employee = Employee::create([
            'name' => 'Marcos Rodrigues',
            'email' => 'marco@kyokugen.org',
            'document' => 71306511054,
            'city' => 'Osasco',
            'state' => 'SC',
            'start_date' => '2021-01-10',
            'user_id' => $this->user->id,
        ]);

        $rows = [
            'name,email,document,city,state,start_date',
            'Marco Rodrigues,marco@kyokugen.org,71306511054,Osasco,SC,2021-02-29',
        ];

        $response = $this->post(route('employees.store'),  [
            'file' => UploadedFile::fake()->createWithContent('employees.csv', implode("\n", $rows))
        ], [
            'Accept' => 'application/json'
        ]);

        $response->assertStatus(406)
            ->assertSee('NOT ACCEPTABLE: Invalid Date');

        $this->assertDatabaseHas('employees', $employee->toArray());
    }
}
