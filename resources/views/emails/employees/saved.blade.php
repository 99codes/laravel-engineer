@component('mail::message')
# Hello {{ $employee->name }}

@if( $employee->wasRecentlyCreated )
You are no registered as an employee.
@else 
Your data as an employee was updated:

{{ 
    collect($employee->getDirty())
        ->map(fn($value, $attribute) => sprintf('%s: %s', $attribute, $value))
        ->implode("\n\n") 
}}
@endif

@endcomponent
